import java.util.Scanner;

public class ox {
static Scanner kb = new Scanner(System.in);
	
	static char[][]board= {
			{' ','1','2','3'},
			{'1','-','-','-'},
			{'2','-','-','-'},
			{'3','-','-','-'}
	};
	
	static int results = 0;
	static int Turn = 1;
	static int AlertBreak = 0;
	
	static int row = 0;
	static int col = 0;
	
	static char player = 0;
	
	
	static void select() {
		System.out.print("Who will Start(X/O) : ");
			String inputString = kb.next();
			if(inputString.equals("X")||inputString.equals("O")) {
				player=inputString.charAt(0);
		}else {
				System.out.println("again please");
			
		}
	}
	static void printWelcome() {
		System.out.println("Welcome to OX Game.");
	}
	
	static void showboard() {
		for(int row = 0;row<board.length;row++) {
			for(int col = 0;col<board[row].length;col++) {
				System.out.print(board[row][col]+" ");
			}System.out.println();
		}
	}
	
	static void printTurn() {
		System.out.println(player + " Turn");
	}
	
	static void input() {
		while (true) {
			System.out.print("Please input row col : ");
			try {
				int row=kb.nextInt();
			int col=kb.nextInt();
			if(board[row][col]=='-') {
				board[row][col]=player;
				break;
			}else {
				System.out.println("input mistakes, and try again.");
			}
			}catch(Exception e) {
				System.out.println("try again");
			}
			
		}
	}
		
	
	static void ChackWin() {
		HorizontalWin();
		VerticalWin();
		DiagonalWin();
		Draw();
	}
	
	static void HorizontalWin() {
		if((board[1][1]==board[1][2]&&board[1][1]==board[1][3]&&board[1][1]!='-')||
			(board[2][1]==board[2][2]&&board[2][1]==board[2][3]&&board[2][1]!='-')||
			(board[3][1]==board[3][2]&&board[1][1]==board[3][3]&&board[3][1]!='-')){
			if(player=='X') {
				results=1;
			}else {
				results=2;
			}
			AlertBreak=1;
		}
	}
	
	static void VerticalWin() {
		if((board[1][1]==board[2][1]&&board[1][1]==board[3][1]&&board[1][1]!='-')||  
			(board[1][2]==board[2][2]&&board[1][2]==board[3][2]&&board[1][2]!='-')||
			(board[1][3]==board[2][3]&&board[1][3]==board[3][3]&&board[1][3]!='-')){
			if(player=='X') {
				results=1;
			}else {
				results=2;
			}
			AlertBreak=1;
		}
	}
	
	static void DiagonalWin() {
		if((board[1][1]==board[2][2]&&board[1][1]==board[3][3]&&board[1][1]!='-')||
			(board[3][1]==board[2][2]&&board[1][3]==board[3][1]&&board[3][1]!='-')){
				if(player=='X') {
					results=1;
				}else {
					results=2;
				}
				AlertBreak=1;
		}
	}
	
	static void Draw() {
		if(Turn==9)
			AlertBreak=1;
	}
	
	static void SwitchTurn() {
		if(player == 'X') {
			player = 'O';
		}else {
			player = 'X';
		}
		Turn++;
	}
	
	static void printWin() {
		if(results==1) {
			System.out.println("X Win");
		}else if(results ==2) {
			System.out.println("O Win");
		}else { 
			System.out.println("Draw");
		}
	}
	
	static void printseeya() {
		System.out.println("see ya");
	}
	

	public static void main(String[] args) {
		printWelcome();
		select();
		while (true) {
			showboard();
			printTurn();
			input();
			ChackWin();
			if(AlertBreak==1) {
				break;
			}
			SwitchTurn();
		}		
		showboard();
		printWin();
		printseeya();

	}
}
